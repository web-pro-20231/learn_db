import { AppDataSource } from "./data-source";
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    const drinkType = await typesRepository.findOneBy({ id: 1 });
    const bekeryType = await typesRepository.findOneBy({ id: 2 });
    const foodType = await typesRepository.findOneBy({ id: 3 });

    const productRepository = AppDataSource.getRepository(Product);
    await productRepository.clear();
    console.log("Inserting a new user into the Memory...");

    var product = new Product();
    product.id = 1;
    product.type = drinkType;
    product.name = "Americano";
    product.price = 40;
    await productRepository.save(product);

    var product = new Product();
    product.id = 2;
    product.type = drinkType;
    product.name = "Espresso";
    product.price = 50;
    await productRepository.save(product);

    var product = new Product();
    product.id = 3;
    product.type = drinkType;
    product.name = "Coco";
    product.price = 50;
    await productRepository.save(product);

    var product = new Product();
    product.id = 4;
    product.type = bekeryType;
    product.name = "Cake1";
    product.price = 70;
    await productRepository.save(product);

    var product = new Product();
    product.id = 5;
    product.type = bekeryType;
    product.name = "Cake2";
    product.price = 70;
    await productRepository.save(product);

    var product = new Product();
    product.id = 6;
    product.type = foodType;
    product.name = "Somtum";
    product.price = 70;
    await productRepository.save(product);

    const products = await productRepository.find({
      relations: { type: true },
    });
    console.log(products);
  })
  .catch((error) => console.log(error));
