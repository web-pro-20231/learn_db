import { AppDataSource } from "./data-source";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    await typesRepository.clear();
    console.log("Inserting a new user into the Memory...");
    var type = new Type();
    type.id = 1;
    type.name = "Drink";
    await typesRepository.save(type);

    var type = new Type();
    type.id = 2;
    type.name = "Bekery";
    await typesRepository.save(type);

    var type = new Type();
    type.id = 3;
    type.name = "Food";
    await typesRepository.save(type);

    const types = await typesRepository.find({ order: { id: "ASC" } });
    console.log(types);
  })
  .catch((error) => console.log(error));
